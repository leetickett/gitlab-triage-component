# GitLab Triage - Workflow Automation

GitLab Triage automates triaging of issues, epics and merge requests using simple
YAML policy files.

> 🚨 🚧 **NOTE** 🚧 🚨 
> 
> This component is **work in progress**, where inputs, template names and the
> entire interface in general may change at any time until version 1.0.0 is reached.

## Usage

Create a `.triage-policies.yml` in the root of the project and use the documentation
from [GitLab Triage](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage).

Create a `.gitlab-ci.yml` to include the component.
A simple configuration should look something like:

```yml
include:
  - component: gitlab.com/components/gitlab-triage/gitlab-triage@main
    inputs:
      api_token: $GUEST_READ_API
      debug: true
      dry_run: true
      job_suffix: ':dry-run'
    rules:
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  - component: gitlab.com/components/gitlab-triage/gitlab-triage@main
    inputs:
      api_token: $DEVELOPER_WRITE_API
    rules:
      - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

A more complex configuration can include additional Gems and helper modules, like:

```yml
include:
  - component: gitlab.com/components/gitlab-triage/gitlab-triage@main
    inputs:
      additional_gems: 'gitlab httparty'
      api_token: $GUEST_READ_API
      debug: true
      dry_run: true
      helpers_file: './helpers.rb'
      job_suffix: ':dry-run'
    rules:
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  - component: gitlab.com/components/gitlab-triage/gitlab-triage@main
    inputs:
      additional_gems: 'gitlab httparty'
      api_token: $DEVELOPER_WRITE_API
      helpers_file: './helpers.rb'
    rules:
      - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

Checkout the sample triage policy and helpers from [this project](https://gitlab.com/components/gitlab-triage).
