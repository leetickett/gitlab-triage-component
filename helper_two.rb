# frozen_string_literal: true

require 'httparty'

module HelperTwo
  def two_method(number_one, number_two)
    number_one + number_two
  end
end
